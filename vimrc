set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
set rtp+=/usr/local/opt/fzf
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'


" Plugins starts

" Gtags
Plugin 'gtags.vim'
Plugin 'rking/ag.vim'
Plugin 'vim-scripts/a.vim'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'w0rp/ale'
Plugin 'keith/swift.vim'
Plugin 'fatih/vim-go'
Plugin 'severin-lemaignan/vim-minimap'
" Recommended by vim-go
Plugin 'garyburd/go-explorer'
Plugin 'python-rope/ropevim'
set rtp+=~/.fzf
Plugin 'junegunn/fzf.vim'
Plugin 'maralla/completor.vim'
Plugin 'sjl/badwolf'
" Plugins end
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

syntax on
let mapleader=","

" Quickly edit/reload the vimrc file
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>

set hidden

set nowrap        " don't wrap lines
set backspace=indent,eol,start
                    " allow backspacing over everything in insert mode
set autoindent    " always set autoindenting on
set copyindent    " copy the previous indentation on autoindenting
set number        " always show line numbers
set shiftwidth=4  " number of spaces to use for autoindenting
set shiftround    " use multiple of shiftwidth when indenting with '<' and '>'
set showmatch     " set show matching parenthesis
set ignorecase    " ignore case when searching
set smartcase     " ignore case if search pattern is all lowercase,
                    "    case-sensitive otherwise
set smarttab      " insert tabs on the start of a line according to
                    "    shiftwidth, not tabstop
set hlsearch      " highlight search terms
set incsearch     " show search matches as you type

set undolevels=1000      " use many muchos levels of undo
set wildignore=*.swp,*.bak,*.pyc,*.class
set title                " change the terminal's title
set visualbell           " don't beep
set ruler

set sts=4
set ts=8
set expandtab
set backupdir=~/.vim/backupfiles//
set directory=~/.vim/swapfiles//
set smartindent
set smartcase
set nocompatible
syntax on

autocmd FileType go set noexpandtab ts=4


set foldmethod=indent
set foldlevel=99

map <leader>g :GundoToggle<CR>

let g:pep8_map='<leader>8'

let g:SuperTabDefaultCompletionType = "context"
set completeopt=menuone,longest,preview

map <leader>n :NERDTreeToggle<CR>

"map <leader>l :RopeGotoDefinition<CR>
"map <leader>r :RopeRename<CR>

set wildmenu
set wildmode=longest:full
set wildignore=*.o,*~,*.pyc,*.pyo
set ignorecase
"set lazyredraw
set magic
set matchtime=2

map j gj
map k gk


if $TMUX != ''
" integrate movement between tmux/vim panes/windows

    fun! TmuxMove(direction)
        " Check if we are currently focusing on a edge window.
        " To achieve that, move to/from the requested window and
        " see if the window number changed
        let oldw = winnr()
        silent! exe 'wincmd ' . a:direction
        let neww = winnr()
        "silent! exe 'wincmd' . oldw
        if oldw == neww
          " The focused window is at an edge, so ask tmux to switch panes
          if a:direction == 'j'
            call system("tmux select-pane -D")
            elseif a:direction == 'k'
              call system("tmux select-pane -U")
            elseif a:direction == 'h'
              call system("tmux select-pane -L")
            elseif a:direction == 'l'
              call system("tmux select-pane -R")
            endif
        else
          "exe 'wincmd ' . a:direction
        end
    endfun

nnoremap <silent> <c-w>j :silent call TmuxMove('j')<cr>
nnoremap <silent> <c-w>k :silent call TmuxMove('k')<cr>
nnoremap <silent> <c-w>h :silent call TmuxMove('h')<cr>
nnoremap <silent> <c-w>l :silent call TmuxMove('l')<cr>
nnoremap <silent> <c-w><down> :silent call TmuxMove('j')<cr>
nnoremap <silent> <c-w><up> :silent call TmuxMove('k')<cr>
nnoremap <silent> <c-w><left> :silent call TmuxMove('h')<cr>
nnoremap <silent> <c-w><right> :silent call TmuxMove('l')<cr>

endif


let mapleader = ","
:nnoremap <leader>ev :vsplit $MYVIMRC<cr>
:nnoremap <leader>sv :so $MYVIMRC<cr>

inoremap jk <esc>
" In insert mode, insert a new line and start directly below last word
"inoremap <c-J> <esc>Bm'o<c-o>100i<space><esc>`'jC
inoremap <c-J> <esc>m>Bm<`>a<cr><c-o>100i<space><esc><esc>`<jcw
noremap <c-J> m>Bm<`>i<cr><c-o>100i<space><esc><esc>`<jdw


let g:CCTreeUseUTF8Symbols = 1

let GtagsCscope_Auto_Load = 1
let GtagsCscope_Auto_Map = 1
let GtagsCscope_Quiet = 1
let GtagsCscope_IgnoreCase = 1
let Gtags_Auto_Update = 1
:nnoremap <leader>l :GtagsCursor<cr>
let Gtags_OpenQuickFixWindows = 0

" uses gscope for tags
set cscopetag

"not open quickfix by default
let Gtags_OpenQuickfixWindow = 0

let g:airline_theme='jellybeans'
"Enable the list of buffers
"let g:airline#extensions#tabline#enabled = 1

" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

"let g:airline_left_sep=''
"let g:airline_right_sep=''
"let g:airline_section_z=''

"Gtags


set cindent
set cino=>s,e0,n0,f0,{0,}0,^0,L-1,:s,=s,l0,b0,gs,hs,ps,ts,is,+s,c3,C0,/0,(0,us,U0,w0,W0,m0,j0,J0,)20,*70,#0

"Python line length
let g:pymode_options_max_line_length = 100
let g:pymode_python = 'python3'


"from old file
"
set modeline
set modelines=5
set textwidth=99
set scrolloff=3
set formatoptions=tcqb
set listchars=tab:>-,eol:$,trail:.,extends:#
set colorcolumn=100
set virtualedit=block
:hi ColorColumn ctermbg=17

set undofile
set undodir=~/.vim/backupfiles
:hi VertSplit ctermfg=DarkYellow
:hi StatusLine ctermfg=DarkYellow


let g:alternateExtensions_cxx = "hxx,h"
let g:alternateExtensions_hxx = "cxx"
let g:alternateNoDefaultAlternate = 1

" Insert a new line and start directly below last word
inoremap <c-J> <esc>m>Bm<`>a<cr><c-o>100i<space><esc><esc>`<jcw
noremap <c-J>       m>Bm<`>i<cr><c-o>100i<space><esc><esc>`<jdw

let g:CCTreeUseUTF8Symbols = 1
nnoremap <leader>l :GtagsCursor<cr>zz
nnoremap <leader>L :vsp<cr><C-W>l:GtagsCursor<cr>zz
nnoremap <leader>h :Gtags<space>-r<cr><cr>zz
nnoremap <leader>H :vsp<cr>:Gtags<space>-r<cr><cr>zz

set cinoptions=s,e0,n0,f0,{0,}0,^0,L-1,:s,=s,l0,b0,gs,hs,N0,ps,ts,is,+s,c3,C0,/0,(0,us,U0,w0,W0,k0,m0,j0,)20,*70,#0


" For minimap
let g:minimap_highlight='Visual'

set history=1000         " remember more commands and search history
colo zellner
"colo elflord
let g:badwolf_darkgutter = 1
"colo badwolf

hi LineNr ctermfg=7

set errorformat=%f(%l)\ :\ %tarning\ C%n:\ %m,%f(%l)\ :\ %trror\ C%n:\ %m

" set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
" set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_go_checkers = ['go', 'golint', 'errcheck']
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_fields = 1
let g:go_highlight_structs = 1
let g:go_highlight_types = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
let g:go_play_open_browser = 0
let g:go_fmt_autosave = 0
let g:go_fmt_fail_silently = 1
let g:go_get_update = 0
let g:go_fmt_command = "goimports"

let g:go_auto_type_info = 1
let g:go_auto_sameids = 1
set autowrite

" we also want to get rid of accidental trailing whitespace on save
autocmd BufWritePre * :%s/\s\+$//e

" tell vim to allow you to copy between files, remember your cursor
" position and other little nice things like that
set viminfo='100,\"2500,:200,%,n~/.viminfo

let g:ale_python_pylint_options = '-rcfile ~/.pylintrc'
let g:ale_python_mypy_options = '--python-version 3.6 --follow-imports silent'
let g:ale_python_flake8_options = '--max-line-length=99 --ignore=E402,E302,E261'

" Integrate default register with OS clipboard
set clipboard=unnamed

" runs a script in a separate pane
nnoremap <leader>run :execute ":silent !tmux respawn-pane -t :0.2 -k 'cd " . getcwd() . ";
    \ ./rerun_when_changed.sh " .  @% .
    \ " ~/proj/learn-python-good/style_exercise_data/medium_sample_data.csv; sleep 5'"<cr>
    \ :redraw!<cr>

let g:completor_python_binary = '/usr/local/bin/python3'
